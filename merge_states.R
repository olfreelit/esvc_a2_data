library("plyr")
setwd("C:/Users/Oscar/Documents/repos/esvc_a2_data")
states <- c("NSW", "VIC", "QLD", "SA", "SNOWY", "TAS")
total = data.frame()
nstates = length(states)
for (s in 1:nstates){
  curstate_df = data.frame()
  for (year in 1998:2016){
    if (year ==1998){
      monthstart =12
    }
    else
    {
      monthstart = 1
    }
    for (month in monthstart:12){
      
      if (month < 10){tmonth = paste0("0",month)}else{tmonth = paste0(month)}
      filename=paste0("csv/DATA",year,tmonth,"_",states[s],"1.csv")
      exists = file.exists(filename)
      
      if (exists == TRUE){
        curdf = read.csv(filename)
        curstate_df= rbind(curstate_df,curdf)
      }
      else{
        print(paste("Missing file:",filename))
      }
    }
    
  }
  write.csv(curstate_df,paste0("result/",states[s],".csv"))
  
