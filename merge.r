require("plyr")
require("SDMTools")
setwd("C:/Users/Oscar/Documents/repos/esvc_a2_data")
states <- c("NSW", "VIC", "QLD", "SA", "SNOWY", "TAS")
totaldf = data.frame()

nstates = length(states)
dfs = list(nstates)
for (s in 1:nstates){
  curstate_df = data.frame()
  for (year in 1998:2016){
    if (year ==1998){
      monthstart =12
    }
    else
    {
      monthstart = 1
    }
    for (month in monthstart:12){
      
      if (month < 10){tmonth = paste0("0",month)}else{tmonth = paste0(month)}
      filename=paste0("csv/DATA",year,tmonth,"_",states[s],"1.csv")
      exists = file.exists(filename)
      
      if (exists == TRUE){
        curdf = read.csv(filename)
        curstate_df= rbind(curstate_df,curdf)
      }
      else{
        print(paste("Missing file:",filename))
      }
    }
    
  }
  dfs[[s]] <- curstate_df
  write.csv(curstate_df,paste0("result/",states[s],".csv"))
  if (empty(totaldf) == TRUE){
    totaldf = curstate_df
  }
  else
  {
    totaldf = rbind(totaldf,curstate_df)
  }
  
}
write.csv(totaldf,"result/total.csv")
meanprice = wt.mean(totaldf$RRP,totaldf$TOTALDEMAND)
meanstd = wt.sd(totaldf$RRP,totaldf$TOTALDEMAND)
for (s in 1:nstates){
  calcmean = wt.mean(totaldf[totaldf$REGION == paste0(states[[s]],"1"),]$RRP,totaldf[totaldf$REGION == paste0(states[[s]],"1"),]$TOTALDEMAND )
  calcstd = wt.sd(totaldf[totaldf$REGION == paste0(states[[s]],"1"),]$RRP,totaldf[totaldf$REGION == paste0(states[[s]],"1"),]$TOTALDEMAND )
  attributes(dfs[[s]]) <- list(mean = calcmean, std = calcstd,regionName = paste0(states[[s]],"1"))
  print(paste(s,"Mean =",calcmean, "; std =",calcstd))
  print(attributes(dfs[[s]]))
}
